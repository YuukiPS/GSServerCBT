FROM ubuntu:latest

RUN export DEBIAN_FRONTEND=noninteractive &&\
    apt update && apt-get upgrade -y && apt-get -y install \
    # dependencies
    git build-essential cmake pkgconf libenet-dev libprotobuf-dev protobuf-compiler liblua5.3-dev nlohmann-json3-dev libreadline-dev \
    # for dispatch server
    python3

WORKDIR /root

COPY soggy-mistress/    ./server/
COPY soggy_resources/   ./server/build/resources/

RUN ls server && cd server &&\
    cmake -B build &&\
    cmake --build build -j8

#RUN cd server/build/ && ./soggy
COPY app.sh ./app.sh

EXPOSE 22102 8099

ENTRYPOINT ["sh", "app.sh"]