// log.hpp

#pragma once

__attribute__((format(printf, 1, 2)))
extern void soggy_log(const char *fmt, ...);
